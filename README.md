# Proyecto

## Project Handbook

Este documento es el manual del proyecto. Aquí se encuentra toda la información
respecto a la forma de trabajo. A lo largo de estos documentos podrás entender
lo que hacemos como equipo. Este manual no representa ningún tipo de
contrato, política empresarial o esquema de trabajo. En lugar de ello debe
considerarse como una guía que nos permite saber en donde estamos, que es lo
que tratamos de conseguir, como pretendemos lograrlo, porque lo estamos
haciendo y, lo más importante de todo, que nos ayude a cumplir nuestras metas.


## Contenido

1. [Introducción](#introducción)
  * [Propósito del documento](#propósito-del-documento)
  * [Uso de este manual](#uso-de-este-manual)
2. [Comunicación](#comunicación)
  * [Chat](#chat)
  * [Documentos](#documentos)
  * [Juntas presenciales](#juntas-presenciales)
3. [Estrategia](#estrategia)
  * [¿Por qué *proyecto*?](#por-qué-proyecto)
  * [Misión](#misión)
  * [Metas](#metas)
  * [Proyección de logros](#proyección-de-logros)
4. [Marketing](#marketing)
  * [Objetivos](#objetivos)
  * [Recursos de marketing](#recursos-de-marketing)


# Introducción

## Propósito del documento

Las principales razones por las cuales existe este documento son:

* Leer es más rápido que escuchar.

* Leer es un proceso individual por lo que no se depende de alguien más para
conocer los objetivos del grupo.

* Tener un manual hace más fácil la capacitación de cualquier persona que se
incorpore al equipo.

* El trabajo es más sencillo cuando se describen, de forma clara y abierta, los
procesos que lo componen.

* El trabajo en equipo es más sencillo cuando se conoce la forma en que
trabajan los demás.

* Los cambios en la forma de trabajar se pueden analizar mejor cuando se
proponen por escrito.

* Todas las discusiones pueden tomar como punto de partida los acuerdos hechos
con anterioridad.

* Todos pueden conocer cualquier decisión tomada cuando esta sea relevante para
el equipo.

Este manual está diseñado para ser usado a largo plazo. Aquí se establecen los
principios básicos bajo los cuales se lleva a cabo el trabajo en equipo. Este
documento puede y debe modificarse tan seguido como sea necesario. Su contenido
**refleja en todo momento** la forma de pensar del equipo en conjunto.

## Uso de este manual

Este manual contiene las reglas básicas bajo las cuales trabaja el equipo.
Antes de tomar alguna decisión o empezar cualquier actividad se debe considerar
lo siguiente:

* Cualquier tema que se discuta entre los miembros del equipo puede ser de
  interés general. Al terminar una conversación se debe determinar donde
  quedarán registradas las conclusiones y quien debe hacerlo.

* Cuando se comparta información siempre se debe tratar de incluir la
  fuente original. Esto permite que el resto del equipo la revise de acuerdo
  a sus propios tiempos. Dependiendo del tipo de información se debe: subir
  archivos a la nube, compartir links, sacar copias de documentos o realizar
  cualquier otro procedimiento necesario para que la información se encuentre
  siempre disponible. En caso de necesitar estos materiales solo será necesario
  preguntarle a la persona responsable donde se pueden encontrar o, si no están
  disponibles, si puede hacerlos públicos.

* Todos los cambios de esta guía serán realizados de forma colaborativa. Los
  cambios se pueden proponer mediate el uso de issues o a través de
  modificaciones directas. Para incluir modificaciones solo será necesario
  crear un merge request con los cambios correspondientes. Dentro del mensaje
  de commit se puede justificar el cambio. Cuando se realice una junta grupal
  se debe mencionar esta edición. Una vez que se anuncie un cambio, y este se
  considere aceptable, una persona distinta a la que lo propuso debe aceptar
  el merge request. Si el cambio se rechaza el merge request se cerrará sin
  aplicarlo.

Recordar que este documento es una guía y, aunque contiene reglas e
indicaciones de trabajo, cada quien puede aplicar su propio criterio a la
hora de hacer las cosas. Un buen motivo para proponer un cambio es encontrar
una mejor forma de realizar el trabajo.

## Comunicación

La comuncación entre Marsbot y *proyecto* se realizará mediante los
siguientes canales: *Poner los medios de comunicaición. Los siguientes son
ejemplos de coordinación*.

### Chat

El *vía* se utilizará de forma grupal para realizar conversaciones de chat.
Se deben considerar lo siguiente:

* Enviar avisos de los eventos y actividades importantes permite que todos
  estemos enterados de lo que ocurre.

* Preguntar por los eventos y actividades permite recordar lo que falta por
  hacer.

* El chat es un medio de comunicación en tiempo real que permite solucionar
  problemas urgentes, por lo que un tema que no es urgente se puede analizar
  con más calma en otro medio.

  * Dentro del chat se pueden proponer o compartir temas de interés para ser
  discutidos. Si un asunto en la conversación pasa de unas cuantas líneas es
  importante considerar si se debe detener el tema y programarlo para una junta.

### Documentos

En la carpeta compartida en Drive se encuentran los siguientes documentos:

* Los articulos para el blog.

* Las fotográfias de *proyecto*.

* Las minutas y temas a tratar en juntas.

### Juntas presenciales

De forma mensual se debe realizar al menos una junta presencial. En estas
juntas debe existir una agenda de temas. Estas juntas son el equivalente a
una junta del consejo directivo donde se toman decisiones respecto a las
acciones que se deben priorizar, discutir cambios drásticos en la forma de
trabajar, se hagan observaciones financieras, se determine el desarrollo y
crecimiento de la empresa, etc.

## Estrategia

### ¿Por qué *Proyecto*?

En esta sección se define la razón por la cual existe el proyeto. La visión del
mismo se debe incluir como un resumen de toda esta explicación.

De forma resumida, **nuestra visión** es la siguiente:

Explicar la motivación del proyecto y como pretende mejorar la situación.

### Misión

La misión del proyecto.

### Metas

1. Enlistar las metas del *proyecto*

2. Pueden ser relacionadas al crecimiento o al impacto social.

### Proyección de logros

Lista de indicadores que permite determinar que se necesita para llegar a las
metas establecidas.

## Marketing

#### Objetivos

Los objetivos del área de marketing son:

* Crear indicadores que permitan medir el crecimiento de la empresa.
 
* Establecer metas mensuales, bimestrales y trimestrales para el 2016 y 2017.
  Las metas mensuales serán la prioridad durante este período de la empresa.
  Estas se pueden modificar en tiempo real para ajustar nuestras expectativas a
  la respuesta del mercado. Es responsabilidad de todo el equipo apoyar y
  aportar los elementos necesarios para lograr alcanzarlas cada que termine
  el mes. Las metas bimestral se utilizarán como indicador de crecimiento
  real. De esta manera se puede revalorar las estrategias utilizadas por la
  empresa y decidir si están siendo de utilidad o no. Las metas trimestrales
  deben de servir como referencia de nuestras propias expectativas, sin
  importar si estas se cumplen o no durante el tiempo establecido.

* Encontrar los diferenciadores y usarlos para mostrar las ventajas que tiene
  el *proyecto* frente a la competencia.

### Nuestra visión del mercado

Un problema en la adquisición del mercado es pensar que los clientes van a
llegar solos. Aunque existe la posibilidad de que nuestros prospectos nos 
al buscar entre las distintas alternativas, una buena estrategia de marketing
nos permite acercanos a ellos. De esta manera logramos crear una base de
usuarios más rápido.

¿Cómo llegar a nuestros clientes? Aunque vale la pena cuestinarse esto, el
verdadero reto es como dar a entender los beneficios que generan nuestros
productos frente a la competencia. Crear una conexión entre nuestros
prospectos y nuestros servicios es el primer paso hacia la adquisición de
clientes. Transmitir de forma clara, directa y precisa las cualidades de
nuestra oferta debe ser la prioridad número uno del equipo de marketing. Antes
de buscar clientes es necesario el poder explicar facilmente lo que nos hace
la mejor opción. De otra forma no sirve de nada tener la atención de estos.

Nuestros segmentos de mercado incluyen a:

* Lista de grupos que conforman los segmentos de mercado.

* En caso de considerar a un único segmento se debe obviar esta lista.

Después de que se hayan establecido los segmentos de mercado se procede a
definir de forma explicita las necesidades y hábitos de consumo de cada uno
para poder crear estrategias funcionales.

### Recursos de marketing

* [**Blog**](handbooks/blog.md): Un blog, como recurso, es el lugar donde se
  concentra toda la información creada como parte de una estrategia de marketing
  con contenidos. En base a los segmentos de mercado se puede definir el tipo de
  contenido que se manejará dentro del blog.

* **Social media**: Tener un alcance dentro de las redes sociales que permita
  difundir los distintos materiales creados en el blog. Las redes sociales
  también tienen como objetivo crear un vinculo con los clientes que permita la
  comunicación constante. Cuando sea posible este medio permitirá crear alianzas
  con otros participantes e influenciadores del sector.

* **Sitio web**: Ofrecer información de forma rápida y accesible sobre nuestros
  servicios a posibles clientes. Además, al incluir el blog, este sitio web se
  convierte en una referencia para nuestros clientes.